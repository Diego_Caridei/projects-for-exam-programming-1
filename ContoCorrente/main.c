//
//  main.c
//  ContoCorrente
//
//  Created by Diego Caridei on 29/01/10.
//  Copyright (c) 2010 Diego Caridei. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "Header.h"
#define NumeroUtenti 10
int main()
{
    menu();

    return 0;
}
void menu(){
    int scelta=0;
    int indiceUtenti=0;
    
    Utente u[NumeroUtenti];
    while (scelta!=4) {
        printf("1) Crea un nuovo utente\n");
        printf("2) Visualizzare la lista movimenti di un utente\n");
        printf("3) Effettuare un operazione sul conto \n");
        printf("4) Per uscire dal programma\n");
       // printf("\u20AC");
        scanf("%d",&scelta);
        switch (scelta) {
            case 1:
                creazioneUtente(&u[indiceUtenti],indiceUtenti);
                indiceUtenti ++;

                break;
            case 2:
                visualizzaUtent(&u[0]);

                break;
                
            case 3:
                operazioneUtente(&u[0]);
                break;
                
            default:
                scelta=0;
                break;
        }
        
        // se gli l'indice degli utenti sono maggiore del size Numero utenti esco
        if (indiceUtenti>NumeroUtenti) {
            printf("Raggiunto il limite massimo degli utenti");
            scelta=4;
        }
        
    }
   
}
void visualizzaUtent(Utente *ute){
    int scelta=0;
    char nome_cognome[10];
    int num_co=0;
    int giorno,mese;
    int i=0;
    while (scelta!=4) {
        printf("Come desideri identificare l'utente?\n1)Tramite Nome\n2)Tramite Cognome\n3)Tramite numero di conto\n4)Torna al menu principale\n");
        scanf("%d",&scelta);
        switch (scelta) {
            case 1:
                printf("inserisci il nome\n");
                scanf("%s",nome_cognome);
                for (i=0; i<=NumeroUtenti; i++) {
                    
                    if (strcmp(ute[i].nome, nome_cognome)==0) {
                        printf("Inserisci il giorno dell'operazione\n");
                        scanf("%d",&giorno);
                        printf("Inserisci il mese dell'operazione\n");
                        scanf("%d",&mese);
                        
                  
                                if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==1) {
                                    printf("Hai effettuato un prelievo\n");
                                    break;
                                }
                            if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==2) {
                            printf("Hai effettuato un versamento\n");
                                break;

                            }
                            else{
                                printf("non hai effettuato nessuna operazione in quel giorno\n");
                                break;

                            }
                        
                            
                        
                        
                        
                        
                    }
                    break;
                case 2:
                    printf("inserisci il cognome\n");
                    scanf("%s",nome_cognome);
                    for (i=0; i<=NumeroUtenti; i++) {
                        
                        if (strcmp(ute[i].cognome, nome_cognome)==0) {
                            printf("Inserisci il giorno dell'operazione\n");
                            scanf("%d",&giorno);
                            printf("Inserisci il mese dell'operazione\n");
                            scanf("%d",&mese);
                            
                            
                            if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==1) {
                                printf("Hai effettuato un prelievo\n");
                                break;

                            }
                            if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==2) {
                                printf("Hai effettuato un versamento\n");
                                break;

                            }
                            else{
                                printf("non hai effettuato nessuna operazione in quel giorno\n");
                                break;

                            }
                            
                            
                            
                            
                            
                        }
                    }
                        break;
                case 3:
                    printf("inserisci il numero di conto\n");
                    scanf("%d",&num_co);
                    for (i=0; i<=NumeroUtenti; i++) {
                        
                        if (ute[i].num_conto==num_co) {
                            printf("Inserisci il giorno dell'operazione\n");
                            scanf("%d",&giorno);
                            printf("Inserisci il mese dell'operazione\n");
                            scanf("%d",&mese);
                            
                            
                            if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==1) {
                                printf("Hai effettuato un prelievo\n");
                                break;

                            }
                            if (ute[i].listaMovimenti.dataOperazione[giorno][mese][0]==2) {
                                printf("Hai effettuato un versamento\n");
                                break;

                            }
                            else{
                                printf("non hai effettuato nessuna operazione in quel giorno\n");
                                break;

                            }
                            
                            
                            
                        }
                    }
                    break;
                    
                default:
                    printf("Opzione non valida\n");
                    break;
                }
                
        }
    }
}


void operazioneUtente(Utente *ute){
    int  giorno, mese;
    int scelta=0;
    int codiceCliente=0;
    float prelievo_deposito;
    int i=0;
    int c;
    while (scelta!=3) {
        printf("Quale operazione vuoi effettuare:\n1)Effettuare un prelievo \n2)Effettuare un deposito\n3)Torna al menu principale\n");
        scanf("%d",&scelta);
        if (scelta==1) {
            printf("Hai deciso di effettuare un prelievo:\n");
            printf("Inserisci il codice\n");
            
            //verifico se viene inserito un intero devo creare una funzione per questo
            do {
                c = scanf ("%d", &codiceCliente );
                if (c == 0) {
                    scanf ("%*[^\n ]");
                    printf (" Attenzione : input non valido .\n ");
                }
            } while (c == 0);

       
            for (i=0; i<=NumeroUtenti; i++) {
                if (ute[i].num_conto==codiceCliente) {
                    printf("Il tuo saldo attuale è:%.2f\n",ute[i].listaMovimenti.capitale);
                    printf("Quanto desideri prelevare?\n");
                    scanf("%f",&prelievo_deposito);
                    
                    if (prelievo_deposito > ute[i].listaMovimenti.capitale) {
                        printf("La scelta non è valida poichè la somma richiesta non è disponibile sul tuo conto\n");
                        break;
                    }
                    if (prelievo_deposito<0) {
                        printf("Valore non valido\n");
                    }
                    else{
                        
                        ute[i].listaMovimenti.capitale=ute[i].listaMovimenti.capitale - prelievo_deposito;
                        printf("inserisci il giorno:\n");
                        scanf("%d",&giorno);
                        printf("inserisci il mese in numero");
                        scanf("%d",&mese);
                        ute[i].listaMovimenti.dataOperazione[giorno][mese][0]=1;
                        // ute[i].listaMovimenti.dataOperazione=
                       // strcpy(ute[i].listaMovimenti.tipoOperazione,"Prelievo");
                        printf("Il tuo saldo attuale è:%.2f\n",ute[i].listaMovimenti.capitale);
                        
                    }
                }
                else{
                    printf("Codice utente non valido\n");
                    break;
                }
            }
            
        }
        if (scelta==2) {
            printf("Hai deciso di effettuare un prelievo:\n");
            printf("Inserisci il codice\n");
            
            do {
                c = scanf ("%d", &codiceCliente );
                if (c == 0) {
                    scanf ("%*[^\n ]");
                    printf (" Attenzione : input non valido .\n ");
                }
            } while (c == 0);
            
            
            for (i=0; i<=NumeroUtenti; i++) {
                if (ute[i].num_conto==codiceCliente) {
                    printf("Il tuo saldo attuale è:%.2f\n",ute[i].listaMovimenti.capitale);
                    printf("Quanto desideri Versare?\n");
                    scanf("%f",&prelievo_deposito);
                    
                    if (prelievo_deposito<0) {
                        printf("Valore non valido\n");
                    }
                    else{
                    
                        printf("inserisci il giorno:\n");
                        scanf("%d",&giorno);
                        printf("inserisci il mese in numero");
                        scanf("%d",&mese);
                        ute[i].listaMovimenti.dataOperazione[giorno][mese][0]=2;
                        ute[i].listaMovimenti.capitale=ute[i].listaMovimenti.capitale + prelievo_deposito;
                       // strcpy(ute[i].listaMovimenti.tipoOperazione,"Versamento");
                        
                        printf("Il tuo saldo attuale è:%.2f\n",ute[i].listaMovimenti.capitale);
                        
                    }
                }
             
                else{
                    printf("Codice utente non valido\n");
                    break;
                }
            }
            
        }
    }
}

void creazioneUtente(Utente *ute, int indice){
    int numeConto=124000;
    int saldo;
    printf("inserisci il nome\n");
    scanf("%s",ute[indice].nome);
    printf("inserisci il cognome\n");
    scanf("%s",ute[indice].cognome);
    ute[indice].num_conto=numeConto +indice;
    printf("inserire un saldo iniziale:\n");
    scanf("%d",&saldo);
    ute[indice].listaMovimenti.capitale=saldo;
    printf("Il numero conto assegnato è:%d\n",ute[indice].num_conto);
}


