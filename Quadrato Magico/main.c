//
//  main.c
//  CuboMagico
//
//  Created by Diego Caridei on 31/01/10.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>//per il quadrato
#include "Header.h"


int main(int argc, const char * argv[])
{
    int **m, i, j,n,loop ;
    int somRiga,somColonna,somDiagonale,magica;
    magica=0;
    somRiga=0;
    somColonna=0;
    somColonna=0;
    srand((unsigned)time(NULL));
    
    printf("Inserire la dimensione della matrice: ");
    scanf("%d", &n);
    int n_quadrato= pow(n,2);
    for (loop=0; loop<=1000; loop++) {
        m=(int **)malloc(sizeof(int *)*n);
        for(i=0; i<n; i++)
            m[i]=(int *)malloc(sizeof(int)*n);
   
        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                m[i][j]=rand() % n_quadrato+1;
                
            }
        }
        stampa(m, n);
        somRiga=somma_riga(m, n);
        somColonna=somma_colonne(m, n);
        somDiagonale=somma_diagonale(m,n);

        if (somColonna==somRiga && somDiagonale==somColonna) {
            printf("matrice magica\n");
            magica++;
        }
    
    }
    printf("le matrici magiche sono:%d\n",magica);
    printf("la percentuale di successi è: %d %% \n",media(magica));
    
    
    
    return 0;
}


//calcolo le somme di ogni riga e le confronto
//se la somma della riga 1,2,...n coincidono ritorno il valore
int somma_riga(int **mat, int n){
    
    int som=0;
    int arraySomme[n];
    int i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            som=som+mat[i][j];
           

        }
         arraySomme[i]=som;
         som=0;

    }
    
    //confronto gli elementi
    int flag=0;
    for (i=0; i<n; i++) {
         if (arraySomme[0] != arraySomme[i]) {
             flag=1;

         }
        
    }

    if (flag==0) {
        
        
         printf("La somma delle righe corrisponde\n");
        return arraySomme[0];
    }
    else{
         printf("le somma delle righe non sono tutte uguali\n");

    }
    
    /*
     stampo il valore delle somme di ogni riga
    for (i=0; i<n; i++) {
        printf("sono il vettore somme %d\n",arraySomme[i]);
    }
     */
    
    return 1;
 
}

int somma_colonne(int **mat, int n){
    
    int som=0;
    int arraySomme[n];
    int i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            som=som+mat[j][i];
            
        }
        arraySomme[i]=som;
        som=0;
        
    }
    
    //confronto gli elementi
    int flag=0;
    for (i=0; i<n; i++) {
        if (arraySomme[0] != arraySomme[i]) {
            flag=1;
            
        }
        
    }
    
    if (flag==0) {
        
        
        printf("La somma delle colonne corrisponde\n");
        return arraySomme[0];
    }
    else{
        printf("le somma colonne non sono tutte uguali\n");
        
    }
    
    /*stampo il valore delle somme di ogni riga
    for (i=0; i<n; i++) {
        printf("sono il vettore somme %d\n",arraySomme[i]);
    }
    */
    
    return 0;
    
}
int media(int vit){
    int med;
    med =(vit*100)/1000;
    return med;
}


int somma_diagonale(int **mat, int n){
    
    int som=0;
    int i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            
            if(i==j){
                som=som+mat[i][j];
            }
            
        }
        
    }
    
    return som;
    
}


//stampa la matrice
void stampa(int **mat, int n)
{
    int i, j;
    
    for (i = 0; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            printf("%d \t", mat[i][j]);
        }
        printf("\n");
    }
}

