//  Created by Diego Caridei on 29/01/10.
//  Copyright (c) 2010 Diego Caridei. All rights reserved.
//
int somma_riga(int **mat, int n);
void stampa(int **mat, int n);
int somma_colonne(int **mat, int n);
int somma_diagonale(int **mat, int n);
int media(int vit);