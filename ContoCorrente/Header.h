//
//  Header.h
//  ContoCorrente
//
//  Created by Diego Caridei on 29/01/10.
//  Copyright (c) 2010 Diego Caridei. All rights reserved.
//
typedef struct utente {
    char nome[10];
    char cognome[10];
    int  num_conto;
    struct{
        float capitale;
        int  dataOperazione[31][12][1];//gironi mesi tipo operazione
    }listaMovimenti;
    
}Utente;
void menu();
void creazioneUtente();
void visualizzaUtent(Utente *ute);
void listaMovimenti(Utente *ute);
void operazioneUtente(Utente *ute);
int verificaInput(int codice);
